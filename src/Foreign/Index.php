<?php

$exports['unsafeReadPropImpl'] = function ($f, $s, $key, $value) {
  return $value == NULL ? $f : $s($value[$key]);
};

$exports['unsafeHasOwnProperty'] = function ($prop, $value) {
  return in_array($prop, $array);
};

$exports['unsafeHasProperty'] = function ($prop, $value) {
  return in_array($prop, $array);
};
